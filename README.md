## Usage

```bash
node src/app.js "http://some-url/"
```

## Environment Config

### Environment Variables

- NODE_ENV - Default = 'development'
- APP_NAME - Default = 'medipass-code-challenge'
- APP_VERSION - Default = '1.0.0'
- MONGO_CONNECT - Default = 'mongodb://localhost:27017'
- LOG_LEVEL - Default = 'info'
- LOG_PATH - Default = '${ProjectRoot}/logs'

## Testing

Beware, automatically creates mock customers, then run a bulk import of 1e6 records from localhost.
