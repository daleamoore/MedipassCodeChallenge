/*jslint node: true */
"use strict";

const { Promise } = require("bluebird");
const { mapSync, split } = require("event-stream");
const { get } = require("http");
const { EnvSettings } = require("./config/config");
const { GetDB } = require("./db/db");
const { Logger } = require("./services/logger");

let ordersCollection;
let customersCollection;
let response;
let orderBuffer = [];
let endOfStream = false;
let done;
let toProcessCount = 0;

GetDB(db => {
  ordersCollection = db.collection("orders");
  customersCollection = db.collection("customers");
});

module.exports.ImportRemoteCSV = (url, cb = () => {}) => {
  done = cb;

  get(url, responseHandler).on("error", err => {
    Logger.error(JSON.stringify(err));

    done();
  });
};

function responseHandler(res) {
  response = res;

  checkResponse(res);

  res
    // splits at a new line
    .pipe(split())
    // run function against each line
    .pipe(mapSync(line => processCSVLine(line)))
    .on("error", error => {
      Logger.error(error);
      done();
    })
    .on("end", () => {
      endOfStream = true;
    });
}

function processCSVLine(line) {
  if (line) {
    toProcessCount++;
    const order = makeOrderFromArray(line.split(","));

    orderBuffer.push(order);

    if (orderBuffer.length === EnvSettings.orderBufferLimit || endOfStream) {
      flushOrderBuffer();
    }
  }
}

function flushOrderBuffer() {
  response.pause();

  customersCollection
    .find({
      customerId: { $in: orderBuffer.map(order => order.customerId) }
    })
    .toArray()
    .then(customers => filterNonExistingCustomers(customers))
    .then(orderBuffer => {
      if (orderBuffer && orderBuffer.length > 0) {
        return ordersCollection.insertMany(orderBuffer);
      }

      return {};
    })
    .then(insertResult => {
      toProcessCount -= orderBuffer.length;
      orderBuffer = [];
      response.resume();
      finishBulkImport();
    })
    .catch(err => {
      Logger.error(err);
      response.resume();
      finishBulkImport();
    });
}

function finishBulkImport() {
  if (toProcessCount === 0 && endOfStream) {
    done();
  }
}

function filterNonExistingCustomers(customers) {
  return Promise.resolve(orderBuffer).filter(order =>
    customers.some(customer => customer.customerId === order.customerId)
  );
}

function makeOrderFromArray([orderId, customerId, item, quantity]) {
  return {
    customerId,
    item,
    orderId,
    quantity: +quantity
  };
}

function checkResponse(res) {
  const { statusCode } = res;
  const contentType = res.headers["content-type"];

  if (statusCode !== 200) {
    throw new Error(`Request failed.\n Status code: ${statusCode}`);
  }

  if (!/text\/plain/.test(contentType)) {
    throw new Error(
      "Invalid content-type.\n Expected text but got " + contentType
    );
  }

  return;
}
