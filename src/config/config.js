/*jslint node: true */
"use strict";
const { normalize } = require("path");

const rootPath = normalize(`${__dirname}/..`);
const projectPath = normalize(`${__dirname}/../..`);
const nodeEnv = process.env.NODE_ENV || "development";
const appName = process.env.APP_NAME || `medipass-code-challenge`;
const appVersion = process.env.APP_VERSION || "1.0.0";
const mongoConnect = process.env.MONGO_CONNECT || "mongodb://localhost:27017";
const logLevel = process.env.LOG_LEVEL || "info";
const logPath = process.env.LOG_DIR || normalize(`${projectPath}/logs`);
const orderBufferLimit = process.env.ORD_BUF_MAX || 1000;

module.exports.EnvSettings = {
  app: {
    name: appName + "-" + nodeEnv,
    version: appVersion
  },
  db: {
    connection: mongoConnect
  },
  environment: nodeEnv,
  log: {
    level: logLevel,
    path: logPath
  },
  orderBufferLimit,
  root: rootPath
};
