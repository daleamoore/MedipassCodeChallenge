/*jslint node: true */
"use strict";

const { readdirSync } = require("fs");

const modelsPath = __dirname + "/models";

module.exports.AddCollections = () => {
  readdirSync(modelsPath).forEach(model => {
    if (model.substr(-3) === ".js") {
      require(`${modelsPath}/${model}`);
    }
  });
};
