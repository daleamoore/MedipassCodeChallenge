/*jslint node: true */
"use strict";

const { MongoClient } = require("mongodb");
const { EnvSettings } = require("../config/config");
const { Logger } = require("../services/logger");
const { AddCollections } = require("./Collections");

let mongoClient;
let db;

function connect(cb) {
  MongoClient.connect(
    EnvSettings.db.connection,
    (err, client) => {
      if (err) {
        throw new Error(JSON.stringify(err));
      }

      Logger.info(`Connected to db @ ${EnvSettings.db.connection}`);

      db = client.db(EnvSettings.app.name);

      db.on("error", error => {
        Logger.error(error);
        process.exit(1);
      });

      AddCollections();

      mongoClient = client;

      cb(db);
    }
  );
}

module.exports.GetDB = cb => {
  if (db) {
    return cb(db);
  } else {
    return connect(cb);
  }
};

module.exports.Close = () => {
  mongoClient.close();
};
