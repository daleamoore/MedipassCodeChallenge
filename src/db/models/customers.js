/*jslint node: true */
"use strict";

const { GetDB } = require("../db");

GetDB(db => {
  db.createCollection("customers", {
    validator: {
      $jsonSchema: {
        bsonType: "object",
        properties: {
          customerId: {
            bsonType: "string",
            description: "must be string and required"
          },
          firstName: {
            bsonType: "string",
            description: "must be string and not required"
          },
          lastName: {
            bsonType: "string",
            description: "must be string and not required"
          }
        },
        required: ["customerId", "firstName", "lastName"]
      }
    }
  });
});
