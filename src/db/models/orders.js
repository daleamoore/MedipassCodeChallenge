/*jslint node: true */
"use strict";

const { GetDB } = require("../db");

GetDB(db => {
  db.createCollection("orders", {
    validator: {
      $jsonSchema: {
        bsonType: "object",
        properties: {
          customerId: {
            bsonType: "string",
            description: "must be string and not required"
          },
          item: {
            bsonType: "string",
            description: "must be string and not required"
          },
          orderId: {
            bsonType: "string",
            description: "must be string and required"
          },
          quantity: {
            bsonType: "number",
            description: "must be string and not required"
          }
        },
        required: ["orderId", "customerId", "item", "quantity"]
      }
    }
  });
});
