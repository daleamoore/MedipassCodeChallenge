/*jslint node: true */
"use strict";

const bunyan = require("bunyan");
const { Writable } = require("stream");
const { EnvSettings } = require("../config/config");

const infoStream = new Writable();
infoStream.writable = true;

infoStream.write = info => {
  console.log(JSON.parse(info).msg);
  return true;
};

module.exports.Logger = bunyan.createLogger({
  name: EnvSettings.app.name,
  streams: [
    { level: EnvSettings.log.level, stream: infoStream },
    { level: "error", path: `${EnvSettings.log.path}/error.log` }
  ]
});
