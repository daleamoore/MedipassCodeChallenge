/*jslint node: true */
"use strict";
const { HttpServer } = require("../__tests__/server/server");
const { ImportRemoteCSV } = require("./BulkImporter");

HttpServer(() => {
  ImportRemoteCSV(process.argv[2], () => {
    console.log("finished");
  });
});
