const { Close, GetDB } = require("../src/db/db");
const { ImportRemoteCSV } = require("../src/BulkImporter");
const { HttpServer, Port } = require("./server/server");

let db;

function createMockCustomers(done) {
  let i = 1e6;
  let customers = [];

  while (i) {
    if (Math.random() < 0.5 ? true : false) {
      customers.push({
        customerId: `custId-${i}`,
        firstName: "foo",
        lastName: "bar"
      });
    }

    i--;
  }

  db.collection("customers").insertMany(customers, (err, result) => {
    done();
  });
}

describe("BulkImport", () => {
  beforeAll(done => {
    GetDB(dbClient => {
      db = dbClient;

      HttpServer();
      createMockCustomers(done);
    });
  }, 10 * 1000);

  afterAll(() => {
    Close();
  });

  it(
    "Should bulk import records",
    done => {
      ImportRemoteCSV(`http://127.0.0.1:${Port}/`, () => {
        db.collection("orders")
          .findOne({})
          .then(order => {
            expect(order).toHaveProperty("quantity");
            done();
          })
          .catch(err => {
            console.log(err);
            done();
          });
      });
    },
    60 * 60 * 1000
  );
});
