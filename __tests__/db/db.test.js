/*jslint node: true */
"use strict";

const { close, getDB } = require("../../src/db/db");

describe("Database", () => {
  let db;

  beforeAll(done => {
    getDB(dbClient => {
      db = dbClient;
      done();
    });
  });

  afterAll(() => {
    close();
  });

  describe("Orders", () => {
    it("Should have a collection named orders", done => {
      db.collection("orders", { strict: true }, (err, collection) => {
        expect(err).toBeFalsy();
        expect(collection).toHaveProperty("collectionName", "orders");
        done();
      });
    });

    it("Should create a document", done => {
      db.collection("orders").insertOne(
        {
          orderId: "foo",
          customerId: "foo",
          item: "foo",
          quantity: 9001
        },
        (err, result) => {
          expect(err).toBeFalsy();
          expect(result).toHaveProperty("insertedCount", 1);
          done();
        }
      );
    });
  });
});
