const { createServer } = require("http");
const { Readable } = require("stream");
const getPort = require("get-port");

let port = 8000;

module.exports.Port = port;

// if (process.env.NODE_ENV === "test") {
//   getPort().then(freePort => (port = freePort));
// }

module.exports.HttpServer = function(cb) {
  const inStream = new Readable({
    read() {}
  });

  const server = createServer();

  server.on("request", (req, res) => {
    res.setHeader("Content-Type", "text/plain");

    inStream.on("end", () => {
      res.end();
    });

    inStream.pipe(res);

    let i = 1e6;
    while (i) {
      inStream.push(`oId-${i},custId-${i},item-${i},${i % 10}`);
      inStream.push("\n");
      i--;
    }

    inStream.push(null);
  });

  server.listen(port);
  console.log(`test server listening on port ${port}`);

  if (cb) cb();
};
